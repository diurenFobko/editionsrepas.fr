const Image = require("@11ty/eleventy-img")
const EleventyVitePlugin = require("@11ty/eleventy-plugin-vite")
const markdownItAttribution = require('markdown-it-attribution')
const markdownItToc = require('markdown-it-toc-done-right')
const markdownItAnchor = require('markdown-it-anchor')
const markdownItAttrs = require('markdown-it-attrs')
const { PATH_PREFIX: pathPrefix = '/', OUTPUT: output = './_site'} = process.env
const group = require('core-js-pure/actual/array/group')
const slugify = require('slug')

module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(EleventyVitePlugin, {
    viteOptions: {
      publicDir: 'assets/public'
    }
  })

  // Default layout for pages
  eleventyConfig.addGlobalData("layout", "layout-base.njk")

  // Copy PDF and images
  eleventyConfig.addPassthroughCopy("Catalogue/**/*.{pdf,PDF}")
  eleventyConfig.addPassthroughCopy("Catalogue/**/*.{JPG,jpg,png,PNG}")
  eleventyConfig.addPassthroughCopy("assets/**/*")
  eleventyConfig.addPassthroughCopy("Images/**/*")

  // Personnalise l'interprétation du Markdown
  eleventyConfig.amendLibrary('md', markdownIt => {
    markdownIt
      .use(markdownItAttribution, {
        marker: '--'
      })
      .use(markdownItAttrs)
      .use(markdownItAnchor)
      .use(markdownItToc, {
        level: 2,
        placeholder: '(\\[\\[\s*sommaire\s*\\]\\])'
      })
  })

  eleventyConfig.addGlobalData('permalink', () => ({ page }) => {
    return (
      page.filePathStem.split('/').map(part => slugify(part)).join('/')
      + (page.filePathStem.endsWith('/index') ? '' : '/index')
      + '.'
      + page.outputFileExtension
    )
  })

  // Tous les ouvrages publiés
  eleventyConfig.addCollection("catalogue", function(collectionApi) {
    return collectionApi.getFilteredByGlob("Catalogue/*/*.md").sort(sortByDepotLegal)

    function sortByDepotLegal(a, b)  {
      return String(b.data.DépotLégal).localeCompare(String(a.data.DépotLégal))
    }
  });

  // Une sélection d'ouvrages publiés, au hasard, avec une bannière graphique
  eleventyConfig.addCollection("sélectionCatalogueBannière", function(collectionApi) {
    return collectionApi.getFilteredByGlob("Catalogue/*/*.md")
      .filter(({ data }) => data['Bannière'])
  });

  // Une sélection ordonnée d'ouvrages à mettre en avant
  eleventyConfig.addCollection("promus", function(collectionApi) {

    const promus = collectionApi.getFilteredByGlob("Catalogue/*/*md");
     // Comment filtrer un tableau
    return promus.filter(bouquin => bouquin.data.AccueilOrdre > 0).sort(function(a, b){
      return (a.data.AccueilOrdre - b.data.AccueilOrdre)
    }).slice(0, 8);
  });

  eleventyConfig.addFilter('concat', (array, ...args) => array.concat(...args))
  eleventyConfig.addFilter('groupByAttribute', (array, attribute) => group(array, (object) => object.data[attribute]))
  eleventyConfig.addFilter('countAttributions', (content) => {
    return Array.from(content.matchAll(/c-blockquote__attribution/g)).length
  })
  eleventyConfig.addFilter("inHours", minutes => Math.round(minutes / 60))
  eleventyConfig.addFilter("inDays", minutes => Math.round(minutes / 60 / 6))


  // Add a new `{% image ... %}` shortcode to resize images in templates
  eleventyConfig.addNunjucksAsyncShortcode("image", async function imageShortcode({ src, alt, width = 600, class: cls = '', loading = 'lazy' }) {
    if (!src) {
      return ''
    }

    if(alt === undefined) {
      // You bet we throw an error on missing alt (alt="" works okay)
      throw new Error(`Missing \`alt\` on myImage from: ${src}`);
    }

    const metadata = await Image(src, {
      widths: [width],
      formats: [null],
      outputDir: `${output}/img/`,
      urlPath: `${pathPrefix}img/`,
    });

    const attributes = {
      alt,
      class: cls,
      loading,
      decoding: 'async',
    }

    return Image.generateHTML(metadata, attributes)
  })

  eleventyConfig.addShortcode('icon', (id, alt = '', className = 'icon') => {
    return `<svg class="${className}" aria-hidden="${alt ? 'false' : 'true'}">
      <title>${alt}</title>
      <use xlink:href="${eleventyConfig.getFilter("url")('/assets/icons/index.svg')}#${id}"></use>
    </svg>`
  })

  return {
    dir: {
      output,
    },
    pathPrefix,
    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk"
  }
};
