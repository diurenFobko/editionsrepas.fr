# Éditions REPAS

Le site web des Éditions REPAS.

**Raccourcis** :

- 📤 [Priorisation des actions à réaliser](https://framagit.org/reseau-repas/editionsrepas.fr/-/boards)
- 👁 [Voir sur le site sur **editionsrepas.fr**](https://editionsrepas.fr)
- ⏳ [Comptabilisation du temps passé](le-temps-qui-passe.csv)

# Manuel d'utilisation du site

## Ajouter une table des matières

Placer le code `[[sommaire]]` dans une page de contenu,
à l'endroit où le sommaire devrait s'afficher.

Le sommaire comptabilise les titres à partir du niveau 2.\
Ainsi, le texte suivant produira le sommaire associé ci-dessous :

```md
# Titre de page

## Sous-titre 1
Du texte

## Sous-titre 2
### Sous-titre 2.1
```

1. Sous-titre 1
2. Sous-titre 2
   - Sous-titre 2.1

## Modifier des pages et contenus

Les contenus sont des fichiers stockés dans le répertoire [`pages`](./pages).

Ils sont écrits avec la **syntaxe Markdown** ([comment écrire avec Markdown ?](https://docs.framasoft.org/fr/grav/markdown.html)).

## Géocoder la liste des points de vente

1. Se rendre sur le fichier [`points-de-vente.csv`](points-de-vente)
2. Cliquer sur l'icône télécharger ![Download](./img/docs/download-icon.png)
3. Modifier le fichier CSV dans un tableur, type OpenOffice¹
4. Se rendre sur [adresse.data.gouv.fr/csv](https://adresse.data.gouv.fr/csv)
5. Déposer le fichier `points-de-vente`
6. Choisir `Adresse`, `Code Postal` et `Ville` dans la section "Choisir les colonnes à utiliser pour construire les adresses"
7. Cliquer sur le bouton "Lancer le géocodage"
8. Télécharger le résultat (`points-de-vente.geocoded.csv`)
9. Cliquer sur le bouton [GitLab "Replace"](points-de-vente)
10. Sélectionner `points-de-vente.geocoded.csv` puis confirmer avec le bouton "Replace file"

¹: Ne pas tenir compte des valeurs de `latitude`, `longitude` et `result_*`


## Mise en ligne automatisée

Elle nécessite de transmettre des "clés SSH" au support ClissXXI. Elles seules sont autorisées à se connecter.

2 variables sont à renseigner dans les [réglages de Framagit](https://framagit.org/reseau-repas/editionsrepas.fr/-/settings/ci_cd#js-general-pipeline-settings) :

- `SSH_PRIVATE_KEY`, en tant que `file` : c'est le contenu d'une clé privée
- `SSH_KNOWN_HOSTS`, en exécutant la commande suivante :
  ```bash
  ssh -i ./deploykey -p 2222 -T editions-repas-site@editionsrepas.fr
  ```
  Où `./deploykey` est le chemin d'accès vers la clé privée précédemment collée.


  ## Redirections de editionsrepas.free.fr vers editionsrepas.fr

<details><pre><code>
Redirect 301 "/editions-repas-editions.html" "https://editionsrepas.fr/catalogue/"
Redirect 301 "/editions-repas-commande.html" "https://editionsrepas.fr/commander/"
Redirect 301 "/editions-repas-evenement.html" "https://editionsrepas.fr/agenda/"
Redirect 301 "/editions-repas-contact.html" "https://editionsrepas.fr/contact/"
Redirect 301 "/editions-repas-points-de-vente.html" "https://editionsrepas.fr/points-de-vente/"
Redirect 301 "/editions-repas-mention.html" "https://editionsrepas.fr/mentions-legales/"
Redirect 301 "/editions-repas-livre-changer-de-direction.html" "https://editionsrepas.fr/catalogue/changer-de-direction/"
Redirect 301 "/editions-repas-livre-vade-mecum-homeopathique-de-l-elevage-en-milieu-pastoral.html" "https://editionsrepas.fr/catalogue/vade-mecum-homeopathique/"
Redirect 301 "/editions-repas-livre-100%25-bio-et-cooperatif.html" "https://editionsrepas.fr/catalogue/100-bio-et-cooperatif/"
Redirect 301 "/editions-repas-livre-cannelle-et-piment.html" "https://editionsrepas.fr/catalogue/cannelle-et-piment/"
Redirect 301 "/editions-repas-livre-une-cite-aux-mains-fertiles.html" "https://editionsrepas.fr/catalogue/une-cite-aux-mains-fertiles/"
Redirect 301 "/editions-repas-livre-la-cantine-des-pyrenees-en-lutte.html" "https://editionsrepas.fr/catalogue/la-cantine-des-pyrenees/"
Redirect 301 "/editions-repas-livre-pour-que-l-argent-relie-les-hommes.html" "https://editionsrepas.fr/catalogue/pour-que-l-argent-relie-les-hommes/"
Redirect 301 "/editions-repas-livre-commun-village.html" "https://editionsrepas.fr/catalogue/commun-village/"
Redirect 301 "/editions-repas-livre-papier-mache.html" "https://editionsrepas.fr/catalogue/papier-mache/"
Redirect 301 "/editions-repas-livre-refaire-le-monde-du-travail.html" "https://editionsrepas.fr/catalogue/refaire-le-monde-du-travail/"
Redirect 301 "/editions-repas-livre-ardelaine-moutons-rebelles.html" "https://editionsrepas.fr/catalogue/moutons-rebelles/"
Redirect 301 "/editions-repas-livre-une-fabrique-de-libertes-le-lycee-autogere-de-paris.html" "https://editionsrepas.fr/catalogue/une-fabrique-de-libertes/"
Redirect 301 "/editions-repas-livre-pour-quelques-hectares-de-moins-tribulations-cooperatives-d-un-vigneron-nomade.html" "https://editionsrepas.fr/catalogue/pour-quelques-hectares-de-moins/"
Redirect 301 "/editions-repas-livre-homeopathie-ferme.html" "https://editionsrepas.fr/catalogue/homeopathie-a-la-ferme/"
Redirect 301 "/editions-repas-livre-soignants-chanteurs.html" "https://editionsrepas.fr/catalogue/soignants-chanteurs/"
Redirect 301 "/editions-repas-livre-entreprenants-associes.html" "https://editionsrepas.fr/catalogue/aux-entreprenants-associes/"
Redirect 301 "/editions-repas-livre-godin.html" "https://editionsrepas.fr/catalogue/godin/"
Redirect 301 "/editions-repas-livre-boimondau.html" "https://editionsrepas.fr/catalogue/faire-des-hommes-libres/"
Redirect 301 "/editions-repas-livre-viel-audon.html" "https://editionsrepas.fr/catalogue/chantier-ouvert/"
Redirect 301 "/editions-repas-livre-tele-millevaches.html" "https://editionsrepas.fr/catalogue/tele-millevaches/"
Redirect 301 "/editions-repas-livre-danse-cep.html" "https://editionsrepas.fr/catalogue/danse-des-ceps/"
Redirect 301 "/editions-repas-livre-ambiance-bois-scions.html" "https://editionsrepas.fr/catalogue/scions-travaillait-autrement/"
Redirect 301 "/editions-repas-livre-cooperer-pour-consommer-autrement.html" "https://editionsrepas.fr/catalogue/cooperer-pour-consommer-autrement/"
Redirect 301 "/editions-repas-livre-autobiographie-raisonnee.html" "https://editionsrepas.fr/catalogue/lautobiographie-raisonnee/"
Redirect 301 "/editions-repas-livre-portraits-faux-la-montagne.html" "https://editionsrepas.fr/catalogue/portraits/"
Redirect 301 "/editions-repas-livre-henri-desroche-esperer-cooperer-s-eduquer.html" "https://editionsrepas.fr/catalogue/henri-desroche/"
Redirect 301 "/editions-repas-parcourir-vie.html" "https://editionsrepas.fr/catalogue/parcourir-sa-vie/"
Redirect 301 "/editions-repas-livre-possible.html" "https://editionsrepas.fr/catalogue/rendre-possible-un-autre-monde/"
Redirect 301 "/editions-repas-livre-economie-sociale.html" "https://editionsrepas.fr/catalogue/leconomie-sociale/"

RedirectMatch 301 "^(.*)$" "https://editionsrepas.fr/$1"
</code></pre></details>
